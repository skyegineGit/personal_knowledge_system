* 查询要安装的java版本
    * yum -y list java*
 * 安装具体java版本
     *  yum install java-1.8.0-openjdk.x86_64
 * 验证 
     * java -version
 * 设置环境变量
        修改 vi /etc/profile在 profile 文件中添加如下内容并保存：
        
        #set java environment
        export JAVA_HOME=/usr/local/jdk1.8.0_91
        export PATH=$PATH:$JAVA_HOME/bin
        export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
        export JAVA_HOME CALSSPATH PATH
  * 刷新环境变量
      * . /etc/profile
  * 再次验证
      * java -version