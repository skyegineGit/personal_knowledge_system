
* 下载MySQL yum源
    wget https://dev.mysql.com/get/mysql80-community-release-el7-1.noarch.rpm
* 安装yum源
    * yum localinstall mysql80-community-release-el7-1.noarch.rpm
* 更新yum源
   * yum clean all
   * yum makecache

* 创建Mysql账户
    * groupadd mysql
    * useradd -g mysql mysql
* 开始安装MySQL
    * yum install mysql-server
* 获取默认密码并修改密码
    * cat /var/log/mysqld.log | grep password
    如果忘记密码，可以编辑vi /etc/my.cnf ，在[mysqld] 下面添加一行 skip-grant-tables  ，然后重启mysql，直接mysql 进入；
    进入 mysql数据库： use mysql;
    mysql8.0 更新密码语句为 update user set authentication_string = '' where user = 'root';
    更新完后 ，编辑 /etc/my.cnf 将skip-grant-tables 注释；
    重启mysql： service mysqld restart
    登录mysql： mysql -u root
    更新密码：ALTER USER 'root'@'%' IDENTIFIED BY 'P@ssw0rd' PASSWORD EXPIRE NEVER;
* 设置远程访问
     update user set host = '%' where user = 'root';
     刷新权限： flush privileges;
       
 至此大功告成!