# 阿里P3C规则
[1.Bug级别](#Bug级别)<br>
[1.1阻断级别](#%e9%98%bb%e6%96%ad%e7%ba%a7%e5%88%ab)<br>
[1.2严重级别](#%e4%b8%a5%e9%87%8d%e7%ba%a7%e5%88%ab)<br>
[1.3主要级别](#%e4%b8%bb%e8%a6%81%e7%ba%a7%e5%88%ab)<br>
[1.4次要级别](#%e6%ac%a1%e8%a6%81%e7%ba%a7%e5%88%ab)<br>
[2.漏洞级别](#漏洞级别)<br>
[3.异味](#异味)<br>
[4.安全热点](#安全热点)<br> 


## Bug级别 
### 阻断级别
1. "@Controller" classes that use "@SessionAttributes" must call "setComplete" on their "SessionStatus" objects

    @Controller方法里面如果使用了@SessionAttributes注解，必须使用setComplete方法关闭，并且Session应该作为参数传递至方法
2. "@SpringBootApplication" and "@ComponentScan" should not be used in the default package

    @SpringBootApplication 和 @ComponentScan 不能使用在一个默认的包文件类上，不然会导致整个文件路径被扫描
3. "PreparedStatement" and "ResultSet" methods should be called with valid indices
   
   使用预处理的SQL查询语句以及结果集需要注意参数的索引，默认应该是从1开始
4. "wait" should not be called when multiple locks are held
    
    当同时持有多有锁时，如果调用wait命令，只会释放其中一个，另一个将一直保留，如果不设置代码请求捕捉该锁，可能就会造成死锁
5. "wait(...)" should be used instead of "Thread.sleep(...)" when a lock is held    

    当前线程持有锁的时候，如果使用Thread.sleep()可能会导致性能问题甚至死锁，应该使用wait()命令释放锁并允许其他进程运行
6. Double-checked locking should not be used

    双重检查锁不应该使用
7. Files opened in append mode should not be used with ObjectOutputStream

    当使用ObjectOutputStream 输出文件的时候，文件流打开不是不能使用追加的方式，因为只有第一个对象才会被读取成功。
8. Loops should not be infinite

    任何循环都不应该是无限的，必须要有结束条件
9. Methods "wait(...)", "notify()" and "notifyAll()" should not be called on Thread instances
    
    不能在线程实例上调用wait()/notify()/notifyAll(),因为这些方法可能会破坏JVM的行为以及不清楚会发生什么未知异常
10. Methods should not call same-class methods with incompatible "@Transactional" values
    
    同一个类里面，方法不能使用不兼容的@Transational方法，这样可能会导致运行异常
11. Printf-style format strings should not lead to unexpected behavior at runtime
    
    使用输出时，格式需要对应，避免和设置的占位符格式不匹配
12. Resources should be closed
    
    使用输入流、输出流以及网络链接等需要及时关闭流和资源
### 严重级别
1. "Random" objects should be reused

    使用Random的时候，每次调用都会创建一个新的实例，并且因为JVM特性获取随机数可能会使用当前时间戳的形式进行获取并非实际随机数，所以使用Random的时候最好使用单例模式获取的随机类方式
2. "runFinalizersOnExit" should not be called

    “runFinalizersOnexit"不应该被调用，因为这个会允许System.runFinalizerOnExit和Runtime.runFinalizerOnExit，但这两个方法本质上是不安全的，如果要使用这个方法，但是这个方法在JVM中使用时可能会因为其他进程占用了，从而导致进程不稳定甚至死锁，如果非要在JVM关闭结果前做些事，应该要附加一个关闭的钩子
3. "ScheduledThreadPoolExecutor" should not have 0 core threads

    使用JVM的任务线程池执行器时，不应该设置coreThreads 为0，因为这会导致执行器不会有任何进程或者运行任何事情
4. "super.finalize()" should be called at the end of "Object.finalize()" implementations
   
   集成父级的finalize()方式，在调用的时候应该注意将这个方法放在最后，避免中途资源进程被结束
5. Dependencies should not have "system" scope
   
   pom文件依赖中不应该有system的区块限制，如果使用system的scope，发布项目的时候可能会导致代码无效
6. Getters and setters should access the expected fields

    类的Get和Set方法应该准确访问到预期的值，简单说不应该使用get方法命名获取A的值，但是返回B的值，应该使用Set Get方法准确地获取期待的值
7. Hibernate should not update database schemas
   
   使用Hibernate时不应该使用update 的模式，因为可能会导致项目中其他正在使用的数据库值被改变或更新，最好是使用validate的方法模式
8. Jump statements should not occur in "finally" blocks
   
   跳转语句比如goto、return 、throw 等，不应该在finally方法代码中存在，因为这个会导致流强制跳出finally块，从而导致一些流未关闭
9. Locks should be released
    
    如果使用锁的时候，不能使用某些判断条件导致锁为能被释放，使用锁，在代码中必须也要释放锁
10. The signature of "finalize()" should match that of "Object.finalize()"
    
    finalize()方法应该要匹配Object.finalize()方法，简单来说就是Object.finalize()方法应该是垃圾回收器当资源为被引用时准备释放资源时调用，如果重载finalize()方法可能会导致两个结果，一是可能重载的方法不会被垃圾回收器调用，二是用户可能会对这个方法感到困惑因为没打算去调用finalize()方法
11. Zero should not be a possible denominator
    
    0 不应该是作为可能的除数，简单来说就是要避免可能会存在的0为除数，这样会直接导致系统异常
### 主要级别
1. ".equals()" should not be used to test the values of "Atomic" classes
   
   AtomicInteger 、AutomicLong 集成于Integer ，但是作用是完全不同的，这些类是用作于处理释放锁以及线程安全变成的单例变量，Automic的类使用equal永远只能等于自身
2. "=+" should not be used instead of "+="
   
   =+ =- 这类的符号不应该使用，应该要使用 += 、-=的形式来替换，因为使用=+这样的计算方式可能会导致得到的结果并非自己预期的结果，比如 int sum = 5;int count =- sum;  实际count结果是-5.
3. "BigDecimal(double)" should not be used
   
   BigDecimal赋值时不应该用new BigDecimal(value)的方式进行新建，因为如果使用这样的方式建立，对于处理double和float值时，因为定义不准确可能会导致实际值的误差，比如new BigDecimal(0.1)可能会导致BigDecimal的值实际可能是0.10232323223234234,这会导致计算的时候会有比较大的误差，建议使用BigDecimal.valueOf(Str),这样使用值比较精确。
4. squid:S4351"compareTo" should not be overloaded
   
   compareTo方法被继承的时候，参数比较的类型应该使用定义在Comparable定义里面的类型，如果使用比较的时候却使用不同类型的参数，很有可能导致非预期的结果
5. "DefaultMessageListenerContainer" instances should not drop messages during restarts
   
   使用默认的消息监听容器的时候，不应该在重启时丢弃消息。当消息接收容器在重启的时候，如果不设置acceptMessagesWhileStopping=true的话，容器在关闭的时候会接收消息，但是不会处理，这会导致这个消息哪怕被接收了，但是永远也不会被处理，造成消息遗失
6. "Double.longBitsToDouble" should not be used for "int"
   
   使用Double的longBitsToDouble 的时候，不应该使用较小的int类型作为参数，因为这会导致结果可能无法预期进行。
7. "equals" method overrides should accept "Object" parameters
   
   重写equal方法的时候必须接收Object的参数，意思就是必须冲写Object参数的equal比较，这样写可以避免任何的疑惑或误解。
8. "Externalizable" classes should have no-arguments constructors
   
   继承类如果需要使用自己的序列化和反序列化，必须有无参构造方法，因为反序列化的时候必须使用它的无参构造方法。
9. "getClass" should not be used for synchronization
    
    getClass这个方法不应该使用synchronize方法进行修饰。除非这个方法被定义为final类型，不然子类集成后可能会导致多个进程都会同时进入到这个代码块，导致syschronize这个标识根本无效。比较好的解决方法是使用类名.getClass（）方法可能会更适用。
10. "hashCode" and "toString" should not be called on array instances
    
    hashCode 和 toString方法不应该在数组实例上调用，因为基本没用，hashCode会返回数组的哈希码，toString返回也是一样的值，两个方法都不能实际反映数组的内容，如果要使用这两个方法应该把数组传递到相关的静态Arrays方法会更合适
11. "InterruptedException" should not be ignored
    
    InterruptedException 不应该在代码中被忽略或仅仅简单记录异常计数。当InteruptedException会清除当前线程被清除的状态，所以如果这个异常没有被合适的处理的话，可能会丢失线程被中断的状态。相反应该在线程异常被清楚后立即再次调用重新抛出中断异常或直接调用系统线程的中断方法，哪怕是单例应用。
12. "iterator" should not return "this"
    
    迭代器的循环中不应该返回this对象，Java对象中有两个实现迭代器的处理方法，两个接口的实现类都不能在方法中返回this
13. "Iterator.hasNext()" should not call "Iterator.next()"
    
    迭代器循环中，不应该调用迭代器的next()方法，因为Iterator.hasNext()不会改变迭代器中任何值，但是如果同时使用Iterator.next()方法时会破坏Iterator.hasNext()方法，并可能会导致未知的异常
14. "notifyAll" should be used
    
    在唤醒睡眠进程时，notify和notifyAll都可以做到，但是notify只能唤醒一个进程，使用notify不一定能唤醒正确的进程，所以notifyAll应该使用
15. "null" should not be used with "Optional"
    
    使用Java8新特性Optional的时候，不应该再接收或返回任何的null，因为optional本身就可以通过ifPresent()方法判断或返回Optional.isEmpty()方法，所以使用Optional类的使用，null不应该在被使用
16. "read" and "readLine" return values should be used
    
    当使用read 或 readLine读取一个资源文件的时候，应该将读取到的值保存，而不是直接进行判断，因为直接读取到的资源不先使用变量存储，而直接进行判断，可能会导致NPE
17. "SingleConnectionFactory" instances should be set to "reconnectOnException"
    
    使用Spring的SingleConectionFactory需要同时允许当异常时重连机制，不然会组织spring的自动重连机制
18. "StringBuilder" and "StringBuffer" should not be instantiated with a character
    
    使用StringBuilder和StringBuffer的时候，不应该用单字符方式进行初始化，简单来说如果想初始化的时候默认是字符串类型的，应该使用双引号，使用单引号的话等于使用单字符对应的int类型数量
19. "Thread.run()" should not be called directly
    
    使用线程的时候，如果是使用Thread的方式创建，应该是使用Thread.start()方法，让start方法启动run，而不应该直接使用Threa.run方法
20. "toString()" and "clone()" methods should not return null
    
    使用toString()方法或者clone()方法不应该返回null值，哪怕真的值为null，应该也需要返回字符串空
21. "volatile" variables should not be used with compound operators
    
    volatile修饰符不能和运算符混合使用，因为使用volatile修饰符修饰的变量在操作的时候不是原子操作，使用volatile修饰的变量如果在进程中操作时交错了，可能会存在数据丢失
22. "wait", "notify" and "notifyAll" should only be called when a lock is obviously held on an object
    
    wait/notify/notifyAll应该对象明显持有锁的时候调用，如果不这样的话，可能会导致IllegaleMonitorException异常，意思说如果需要调用这三个方法，尽量在synchronized语句块中使用
23. A "for" loop update clause should move the counter in the right direction
    
    在循环中，更新下标的值应该是往正确的方向移动，需要递增或递减，避免出现数组越界的问题。
24. All branches in a conditional structure should not have exactly the same implementation
    
    在一个switch 和 if分支中，如果所有的分支都最后实现了同一个方法，要么是代码复制错误，要么就不应该有switch 或 if 分支
25. Blocks should be synchronized on "private final" fields
    
    使用synchronized同步方法时，应该是同步在一个定义为private final的对象上，这样可以避免如果标识一个变量，但变量同时在别的线程中更改时，可能会导致synchronized无效
26. Child class methods named for parent class methods should be overrides
    
    子类方法拥有和父类相同方法名时应该要重写父类的方法，但是如果出现以下三种情况时，可能父类方法在子类中不被重写。一是 父类中方法是private，二是父类中方法是静态的，三世父类中相同方法的入参和子类的方法入参是引入来自不同的包下面，但是相同类名。
27. Classes extending java.lang.Thread should override the "run" method
    
    集成Thread的类应该要重写run方法，集成Thread类但是不重写run方法没有任何意义，并且容易导致不太好理解这个方法作用
28. Classes should not be compared by name
    
    不应该使用类名来进行比较或判断两个类是否是一致的，可以通过instanceOf 方法 或Class.isAssignableFrom(AClass)进行判断
### 次要级别

## 漏洞级别
## 异味
## 安全热点
