## 记录MySQL 8.0.15 部署在Azure服务器上，数据库连接的坑

#### 基础配置

 * 1. MySQL 8.0.15
 * 2. mysql-connector-java 8.0.15
 * 3. 服务器：Azure

	使用的ssm项目，在本地测试完全没有问题，实际部署到服务器上时出现 异常：unknow system variable "query_cache_size"; 排查后发现是Mysql 8后放弃了这个参数，网上各个版本问题答案都是说要提高mysql-connector-java 版本，实际没有效果。
	多次排查后在Azure官方文档上发现，Azure 使用mysql数据库必须使用org.mariadb.jdbc数据库作为jdbc的driver，后来更改了jdbc连接驱动，具体配置如下：


```
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
spring.datasource.url=jdbc:mysql://xx.xx.xx.xx:3306/dbname?useSSL=false&useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC
spring.datasource.username=
spring.datasource.password=
```

  满怀希望重新启动，发现还是不行，报错信息为 unknow system
variable "tx_isloation"   显然我们开了事务，mysql8.0 将 tx_isloation 换了个新名字 transcation_isloation(TM无聊)，也是个明显驱动版本过低的问题，但是坑又来了，mariadb默认使用 ***HikariCP*** 作为数据库连接池。。坑坑坑！！！！ 后来改成了Druid连接池，问题终于终于终于解决了。
	顺便补充下：部署到Azure 的项目尽量不要使用mybatis-plus ，mybatis-plus 目前不支持Mariadb
