### 问题产生
    在项目开发中，有一次的业务需求是需要对订单列表中信息进行处理，同时保留原订单信息列表，简单直接上手撸，在对原订单List<Order>中值进行操作时，最后发现复制出来的List内的对象值也同时改变。第一次写的代码如下
```
Map<String,Object> resMap = new HashMap<>();
List<Order> orderList = getOrder(queryDto);
// 记录原单据
List<Order> orginalOrderList = new LinkedList<>();
orginalOrderList.addAll(orderList);
// 保存原单据
resMap.put("orginalOrderList",orginalOrderList);

// orderList.foreach(order -> {
       // 模拟业务处理
        order.setBillingNo(billingNo);
        ...
   });

// 保存处理后单据
resMap.put("orderList",orderList);
```

在处理完业务后，发现保存的原单据内的值也同时改变了，和新单据一致，后来改成了将原单据List 定义为final类型的，但是还是不起效果，后来才想起来，List中值为对象，并非基础数据类型，这些对象对应的内存中地址是一致的，单据改变后，内存地址并没有改变，所以导致保存的原单据值也是同样改变了，后来查询了下，找到了解决方案，使用BeanUtils.clonebean(Object object) ，解决了该问题

### 解决方案
修改后代码如下
```
Map<String,Object> resMap = new HashMap<>();
List<Order> orderList = getOrder(queryDto);
// 记录原单据
List<Order> orginalOrderList = new LinkedList<>();


// orderList.foreach(order -> {
        // 主要解决代码
        orginalOrderList.add((Order)BeanUtils.cloneBean(order));

       // 模拟业务处理
        order.setBillingNo(billingNo);
        ...
   });

// 保存原单据
resMap.put("orginalOrderList",orginalOrderList);
// 保存处理后单据
resMap.put("orderList",orderList);
```

### 浅克隆深度克隆
上述问题产生的主要原因就是因为在对对象进行复制操作，其实本质上的问题是因为使用的是浅克隆问题。

Java中有深度克隆和浅克隆的说法，比如数组复制、实现CloneAble接口，其本质上就是浅克隆。浅克隆如果对于基本类型而言是没有什么问题的，克隆的时候会直接把基本类型复制到克隆的对象里面。但如果要复制的对象包含了引用类型对象，上述方法只会在克隆的时候将被克隆对象的引用指向克隆对象，新旧对象的引用在内存中还是同一个。
深度克隆是指在克隆的时候将对象的所有属性复制一份到新对象中去，不仅是将被克隆对象引用赋值给新对象。
   
### 强弱软虚引用
1. 强引用

      

2. 弱引用
3. 软引用
4. 虚引用（幽灵引用）

### BeanUtil.clone()方式实现原理

TODO 其他工具类是否有类似问题，BeanUtil实现方式，强弱引用


记录下解决问题的方案，方便以后能用到，如果能够对你也有所帮助，那是更好
