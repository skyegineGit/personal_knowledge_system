
# JDK8新特性

1. [Lambda](#1)
2. [Stream](#2)
3. [Optional](#Optional)

## Lambda

#### What?
Lambda表达式是一种***函数式编程***方式，最主要的功能是可以把函数作为参数传递到方法中，使用Lambda可以让代码变得比较简洁<br>
* 函数式接口概念
1. 只包含一个抽象方法的接口
2. 可以通过Lambda 表达式来创建该接口的对象
* 比较常见的函数式接口
      Runnable 接口、Compatator接口
* 语法格式
```
    (param) -> expression
    (param) -> {statements;}

    eg: List<String> names = Arrays.asList("hello","world","nice","meet","you");
        names.forEach(name -> System.out.print(name));
    或者
        names.forEach(System.out::println);
```
| 类别         | 使用形式           |
| :----------- | :----------------- |
| 静态方法引用 | 类名::静态方法名   |
| 实例方法引用 | 实例名::实例方法名 |

#### How?
Lambda 表达式一般比较适合结合Stream使用或用来使用匿名内部类，就目前而言，经常使用的地方是在对List进行操作时，使用stream流使用lambda表达式优化写法并且提高代码效率，比如
```
 1. 通过stream 结合lambda 可以快速获取list中某个字段集合list
 List<String> buyerTaxList = orderList.stream().map(OrderMainAdvanced::getBuyerTaxNum).distinct().collect(Collectors.toList());
 2. 可以通过stream的filter表达式，对list进行过滤筛选出满足条件的list
 List<OrderMainAdvanced> orderMainAdvancedList = orderList.stream().filter(orderMainAdvanced -> buyerTax.equals(orderMainAdvanced.getBuyerTaxNum())).collect(Collectors.toList());
            
```

#### Why?
就目前而言，个人对lambda的表达式从使用方面可以简化代码的书写，并且能够结合stream操作或匿名内部类的方式实现快速编写代码，完成部分原本需要冗长的代码才能完成的功能，至于具体更多详细的功能还需要在使用中更多发掘


TODO Lambda表达式更多使用

## Stream

### What？
**Stream **是JDK8 新出的一个特性，主要是流操作，类似于原本的文件流。流的出现对于原本数组List操作性能操作是比较大的提升，可以快速对集合进行查找、遍历、计算、排序等操作。<br/>
Stream流目前主要有两种，分别是**parallelStream**和**stream**,两者区别在于**parallelStream**是多线程的，但是并**非线程安全**，但效率会比较高，比较适合于不在意线程安全的List操作。**Stream**是单线程，线程安全，目前使用较多。

### How？
Stream使用主要是针对于集合进行操作，主要使用方法如下

1. 过滤出集合中等于某个满足条件的集合
```
List<OrderMainAdvanced> orderMainAdvancedList = orderList.stream().filter(orderMainAdvanced -> buyerTax.equals(orderMainAdvanced.getBuyerTaxNum())).collect(Collectors.toList());
```
2. 对集合中对象属性值取出去重后生成新集合
```
List<String> buyerTaxList = orderList.stream().map(OrderMainAdvanced::getBuyerTaxNum).distinct().collect(Collectors.toList());
```
3. 对集合中对象或属性值进行累计计算
```
Double totalTax = BigDecimal.valueOf(orderList.stream().mapToDouble(OrderMainAdvanced::getTax).sum()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
```
4. 匹配判断 anyMatch /noneMatch /allMatch
```
boolean existMinusDetail = orderDetailAdvancedList.stream().anyMatch(orderDetailAdvanced -> orderDetailAdvanced.getTaxMoney() < 0);
```
5. 排序
```
List<OrderDetailAdvanced> minusGoodsNameEqualList = orderDetailAdvancedList.stream().filter(orderDetailAdvanced -> orderDetailAdvanced.getGoodsName().equals(minusDetail.getGoodsName())).sorted(Comparator.comparing(OrderDetailAdvanced::getTaxMoney).reversed()).collect(Collectors.toList());
```
6. count/min/max 对集合中计算计算、取最小值、最大值等操作          

### Why？
就目前使用而言，使用Stream流对集合进行操作时，可以比较快捷的取出或过滤、匹配出需要的结果，并且结合Lambda表达式使用的情况下，代码会显得比较简洁，对集合的操作也是比较方便，个人而言比较喜欢使用这个


## Optional
### What?
    Optional是Java中对于经常出现的NPE异常进行处理的新特性，Optional可以存储类型为T的值，同时也可以存储null，使用Optional可以在程序中优化简化代码，并且比较优雅的判断空值问题
### How?
 Optional.ofNullable()

    使用Optional.ofNullable可以对传入的对象值进行保存并返回，Option<对象>类型值,通过返回的值可以进一步做下列方法操作
     Company company = new Company();
     Optional<Company> company1 = Optional.ofNullable(company);
 Optional.ifPresent()

    对返回的Optional对象可以调用其ifPresent()方法对其判断是否为空
    if(company1.isPresent()){
        System.out.println("companyName:"+company1.get().getCompanyName());
    }
 Optional.orElse(value)
    
        对比ofNullable方法 orElse会对对象值进行判断，如果不为空，获取原对象值，如果为空以orElse中值为准
 Optional.orElseThrow(Exception)

        orElseThrow操作为当对象为null时，直接抛出一个异常
 Optional.ofNullable(对象).map()

    使用Optional.ofNullable(对象).map(实例::属性).orElse(""),可以获取对象的属性值并返回,如果为空的话，返回else中设定的值
### Why?
